import sys,random
import pygame


class Jeu:
    def __init__ (self):
        #initialisation des variables

        #lxL de l'ecran
        self.ecran = pygame.display.set_mode((800,600))
        pygame.display.set_caption('Snake')

        #si le jeu en cours est vrai
        self.jeu_encours = True

        #creation de la position et direction du serpent
        self.serpent_position_x = 300
        self.serpent_position_y = 300
        self.serpent_direction_x = 0
        self.serpent_direction_y = 0

        self.serpent_corps = 10

        #position de la pomme
        self.pomme_position_x = random.randrange(110,690,10)
        self.pomme_position_y = random.randrange(110,590,10)
        self.pomme = 10

        #fps
        self.clock = pygame.time.Clock()

        #cree une liste des postion du serpent
        self.position_serpent = []

        #la taille du serpent
        self.taille_du_serpent = 1

        self.ecran_accueil = True
        #charger l'image du jeu
        self.image = pygame.image.load('Background.jpg')
        #crop l'image
        image= pygame.transform.scale(self.image, (800, 600))

        #cree le score
        self.score = 0

    def fonction_principale(self):
        while self.ecran_accueil:
            for evenement in pygame.event.get():
               # print(evenement)
                if evenement.type == pygame.QUIT:
                    sys.exit()

                if evenement.type == pygame.KEYDOWN:
                    if evenement.key == pygame.K_RETURN:
                        self.ecran_accueil = False

                self.ecran.fill((0,0,0))
                self.ecran.blit(self.image,(-200,-100,0,0))

                self.create_message('moyenne', 'Appuyez sur Enter pour jouer', (200, 450, 200, 5), (255, 255, 255))


                pygame.display.flip()

        #gerer les evenement du jeu
        while self.jeu_encours:
            for evenement in pygame.event.get():
               # print(evenement)
                if evenement.type == pygame.QUIT:
                    sys.exit()

                if evenement.type == pygame.KEYDOWN:

                    if evenement.key == pygame.K_RIGHT:
                        self.serpent_direction_x = 10
                        self.serpent_direction_y = 0

                    if evenement.key == pygame.K_LEFT:
                        self.serpent_direction_x = -10
                        self.serpent_direction_y = 0

                    if evenement.key == pygame.K_DOWN:
                        self.serpent_direction_y = 10
                        self.serpent_direction_x = 0

                    if evenement.key == pygame.K_UP:
                        self.serpent_direction_y = -10
                        self.serpent_direction_x = 0



            #collision entre les limites
            if self.serpent_position_x <= 100 or self.serpent_position_x >= 700 or self.serpent_position_y <= 100 or self.serpent_position_y >= 600:
                sys.exit()

            self.mouvement_serpent()


            if self.pomme_position_y == self.serpent_position_y and self.serpent_position_x == self.pomme_position_x:
               # print('ok')
                self.pomme_position_x = random.randrange(110,690,10)
                self.pomme_position_y = random.randrange(110,590,10)
                #augmenter la taille du serrpent

                self.taille_du_serpent += 1

                #augmenter le score
                self.score += 1

            ##cree tete du serpent

            la_tete_du_serpent = []
            la_tete_du_serpent.append(self.serpent_position_x)
            la_tete_du_serpent.append(self.serpent_position_y)

            if len(self.position_serpent) >=  self.taille_du_serpent:
                self.position_serpent.pop(0)


            #affichage des éléments
            self.affichage()
            self.se_mord(la_tete_du_serpent)
            self.create_message('grande','Snake Game', (320,10,100,50), (255,255,255),)
            self.create_message('grande', '{}'.format(str(self.score)), (375,50,50,50), (255,255,255), )


            self.position_serpent.append(la_tete_du_serpent)

            #afficher les limites
            self.creer_limite()
            self.clock.tick(20)
            pygame.display.flip()

    def creer_limite(self):
        pygame.draw.rect(self.ecran,(255,255,255), (100,100,600,500),3)

    def mouvement_serpent(self):
        # faire bouger le serpent
        self.serpent_position_x += self.serpent_direction_x
        self.serpent_position_y += self.serpent_direction_y

        # print(self.serpent_position_x, self.serpent_position_y)
    def affichage(self):
        # couleur de l'ecran
        self.ecran.fill((0, 0, 0))

        # afficher le serpent
        pygame.draw.rect(self.ecran, (0, 255, 0),
                         (self.serpent_position_x, self.serpent_position_y, self.serpent_corps, self.serpent_corps))

        # afficher la pomme
        pygame.draw.rect(self.ecran, (255, 0, 0),
                         (self.pomme_position_x, self.pomme_position_y, self.pomme, self.pomme))
        self.afficher_serpent()

    def afficher_serpent(self):
        # afficher les autre parties du serpent
        for partie_du_serpent in self.position_serpent:
            pygame.draw.rect(self.ecran, (0, 255, 0),
                             (partie_du_serpent[0], partie_du_serpent[1], self.serpent_corps, self.serpent_corps))

    def se_mord(self, la_tete_du_serpent):
        # si le serpent se mord la queue on arrete le jeu
        for partie_du_serpent in self.position_serpent[:-1]:
            if la_tete_du_serpent == partie_du_serpent:
                sys.exit()

    def create_message(self,font,message,message_rectangle,couleur):
        if font == 'petite':
            font = pygame.font.SysFont('Lato', 20, False)
        elif font == 'moyenne':
            font = pygame.font.SysFont('Lato', 30, False)
        elif font == 'grande':
            font = pygame.font.SysFont('Lato', 40, True)

        message = font.render(message,True,couleur)
        self.ecran.blit(message, message_rectangle)

if __name__ == '__main__':
    pygame.init()
    Jeu().fonction_principale()
    pygame.quit()

